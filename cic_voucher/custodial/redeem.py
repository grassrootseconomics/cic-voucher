# standard imports
import logging

# external imports
from cic_eth.api import Api

#logg = logging.getLogger(__name__)
logg = logging.getLogger()


class PassthroughGetter:

    def get(v):
        return v


class CustodialRedeemer:

    def __init__(self, chain_spec, voucher_spec, voucher_getter, queue='cic-eth', recipient_getter=PassthroughGetter):
        self.chain_spec = chain_spec
        self.queue = queue
        self.voucher_getter = voucher_getter
        self.voucher_spec = voucher_spec
        self.recipient_getter = recipient_getter
        self.api = Api(str(self.chain_spec), callback_param='transfer_from', callback_task='cic_eth.callbacks.noop.noop', queue=None)
   

    def redeem_custodial(self, k, recipient, spender=None, session=None):
        voucher = self.voucher_getter.get(k, session=session)
        holder = voucher.holder_address
        if spender == None:
            spender = voucher.spender_address
        if spender == None:
            ValueError('no spender set')
        value = voucher.value
        symbol = voucher.token
        recipient = self.recipient_getter.get(recipient)

        t = self.api.transfer_from(holder, recipient, value, symbol, spender)
        return t
