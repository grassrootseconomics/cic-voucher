# standard imports
import logging

# external imports
from cic_eth.error import YouAreBrokeError
from eth_erc20 import ERC20
from chainlib.eth.pytest.fixtures_signer import agent_roles
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import receipt

# local imports
from cic_voucher.custodial import (
        VoucherBatch,
        VoucherStaticValueGenerator,
        )
from cic_voucher.custodial.redeem import CustodialRedeemer
from cic_voucher.error import DoubleSpendError

# test imports
from tests.cic_voucher_local.util import (
        VoucherLimitStore,
        )
from tests.fixtures_celery import *

logg = logging.getLogger()


def test_double_spend(
        default_chain_spec,
        contract_roles,
        default_voucher_spec,
        ):

    generator = VoucherStaticValueGenerator(1024, 10)
    voucher_batch = VoucherBatch(default_voucher_spec, generator, bitsize=8)
    voucher_store = VoucherLimitStore()
    voucher_batch.generate(voucher_store.callback)

    v = voucher_store.__iter__().__next__()
    k = str(v)

    voucher_store.get(k)
    with pytest.raises(DoubleSpendError):
        voucher_store.get(k)


def test_redeem_custodial(
        default_chain_spec,
        contract_roles,
        default_voucher_spec,
        celery_worker,
        foo_token,
        custodial_roles, 
        agent_roles,
        init_database,
        eth_rpc,
        eth_signer,
        ):

    generator = VoucherStaticValueGenerator(10, 10)
    voucher_batch = VoucherBatch(default_voucher_spec, generator, bitsize=8)
    voucher_store = VoucherLimitStore()
    voucher_batch.generate(voucher_store.callback)
    
    redeemer = CustodialRedeemer(default_chain_spec, default_voucher_spec, voucher_store)
    ks = list(voucher_store.store.keys())
    t = redeemer.redeem_custodial(ks[0], agent_roles['BOB'], spender=agent_roles['ALICE'])
    
    with pytest.raises(YouAreBrokeError):
        t.get_leaf()

    voucher = voucher_store.peek(ks[0])
    holder = voucher.holder_address
    spender = voucher.spender_address
    value = voucher.value

    nonce_oracle = RPCNonceOracle(holder, conn=eth_rpc)
    c = ERC20(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle) 
    (tx_hash, o) = c.approve(foo_token, holder, agent_roles['ALICE'], value * 2)
    r = eth_rpc.do(o)
    o = receipt(tx_hash)
    r = eth_rpc.do(o)
    assert r['status'] == 1

    t = redeemer.redeem_custodial(ks[1], agent_roles['BOB'], spender=agent_roles['ALICE'])
    t.get_leaf()
    assert t.successful()

    recipient_map = {
            'bob': agent_roles['BOB'],
            }

    redeemer = CustodialRedeemer(default_chain_spec, default_voucher_spec, voucher_store, recipient_getter=recipient_map)
    t = redeemer.redeem_custodial(ks[2], 'bob', spender=agent_roles['ALICE'])
