# standard imports
import uuid
import datetime


class VoucherSpec:

    def __init__(self, chain_spec, token_symbol, holder_address, label=None, prefix=None, valid_seconds=None):
        if prefix == None:
            prefix = ''
        elif not isinstance(prefix, str):
            raise ValueError('prefix must be string')

        self.chain_spec = chain_spec
        self.token_symbol = token_symbol
        self.holder_address = holder_address
        self.prefix = prefix
        if label == None:
            if self.prefix != None:
                label = prefix
            label = str(uuid.uuid4())
        self.label = label
        self.expires = None
        if valid_seconds != None:
            self.expires = datetime.datetime.utcnow() + datetime.timedelta(valid_seconds)
