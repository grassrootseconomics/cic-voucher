def number_filter(v):
    if not isinstance(v, bytes):
        raise ValueError('identifier filter takes bytes only, and was given {}'.format(type(v).__name__))
    n = int.from_bytes(v, byteorder='big')
    return str(n)


class PadFilter:

    def __init__(self, pad_size, fltr):
        self.pad_size = pad_size
        self.fltr = fltr
        self.fmt = '{' + ':>0{:d}s'.format(self.pad_size) + '}'

    def filter(self, v):
        v = self.fltr(v)
        if len(v) > self.pad_size:
            raise ValueError('value {} exceeds pad_size {}'.format(v, self.pad_size))
        return self.fmt.format(v)


