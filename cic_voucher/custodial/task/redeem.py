# external imports
import logging
import celery

# local imports
from cic_voucher.error import DoubleSpendError

#logg = logging.getLogger(__name__)
logg = logging.getLogger()

celery_app = celery.current_app


class VoucherTask(celery.Task):
    session_func = None
    redeemer = None


@celery_app.task(bind=True, base=VoucherTask, throws=(DoubleSpendError,))
def redeem_voucher(self, voucher_key, recipient):
    session = VoucherTask.session_func()
#    try:
#        voucher = VoucherTask.get(voucher_key, session=session)
#    except DoubleSpendError as e:
#        logg.exception(e)
#        session.close()
#        raise e
#    session.close()

    #if voucher == None:
    #    session.close()
    #    raise ValueError('no such voucher {}'.format(voucher_key))

    VoucherTask.redeemer.redeem_custodial(voucher_key, recipient, session=session)
    session.close()
