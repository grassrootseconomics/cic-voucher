# external imports
import pytest
import tempfile
import logging
import shutil

# local impors
from cic_eth.pytest.fixtures_celery import *

logg = logging.getLogger(__name__)


# celery fixtures
@pytest.fixture(scope='session')
def celery_includes():
    return [
        'cic_eth.eth.erc20',
        'cic_eth.admin.ctrl',
        'cic_eth.callbacks.noop',
        'cic_eth.queue.state',
        'cic_voucher.custodial.task.redeem',
    ]
