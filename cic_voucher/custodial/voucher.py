# standard imports
import logging
import math
import os
import datetime

# local imports
from cic_voucher.error import InvalidError
from cic_voucher import VoucherSpec
from cic_voucher.filter import number_filter
from cic_voucher.custodial.state import VoucherState

logg = logging.getLogger()


class Voucher:

    def __init__(self, identifier, value, spec, spender_address=None):
        self.identifier = identifier
        if not isinstance(spec, VoucherSpec):
            raise TypeError('spec object must be subclass of cic_voucher.VoucherSpec')
        self.spec = spec
        self.value = value
        if spender_address == None:
            spender_address = self.spec.holder_address
        self.spender_address = spender_address
        self.status = VoucherState.REDEEMABLE


    def set_used(self):
        if self.status != VoucherState.REDEEMABLE:
            raise StateChangeError('voucher must have state {} to set {}, had {}'.format(
                VoucherState.REDEEMABLE,
                VoucherState.USED,
                self.status,
                )
                )
        self.status = VoucherState.USED


    def check_used(self):
        return self.status & VoucherState.USED > 0


    def check_expired(self):
        if self.spec.expires != None:
            if self.spec.expires < datetime.datetime.utcnow():
                self.set_expired()
                return True
        return False


    def set_expired(self):
        self.status &= 0xfe
        self.status |= VoucherState.EXPIRED


    def set_revoked(self):
        self.status &= 0xfe
        self.status |= VoucherState.REVOKED


    @property
    def holder_address(self):
        return self.spec.holder_address


    @property
    def token(self):
        return self.spec.token_symbol


    def __str__(self):
        return self.spec.prefix + str(self.identifier)


    def __repr__(self):
        return 'voucher label {}: prefix {} id {}'.format(self.spec.label, self.spec.prefix, str(self.identifier))


    def asdict(self):
        return {
                'identifier': self.identifier,
                'token': self.spec.token_symbol,
                'value': self.value,
                'holder': self.spec.holder_address,
                'spender': self.spender_address,
                'prefix': self.spec.prefix,
                'label': self.spec.label,
                }


class VoucherStaticValueGenerator:

    def __init__(self, value, limit):
        self.value = value
        self.limit = limit


    def count(self):
        return self.limit


    def next(self):
        return self.value


class VoucherBatch:

    strikes_limit = 3

    def __init__(self, spec, value_generator, bitsize=64, identifier_filter=number_filter):
        if bitsize < 1:
            raise ValueError('bitsize must be positive value')
        if not callable(identifier_filter):
            raise TypeError('identifier filter must be callable')
        self.spec = spec
        self.value_generator = value_generator
        self.number_generated = 0
        self.fltr = identifier_filter
        self.amount = self.value_generator.count()

        amount_bitsize = math.ceil(math.log2(self.amount))
        if bitsize <= amount_bitsize:
            logg.critical('not enough bits to generate voucher amount; required {} but have {}'.format(amount_bitsize, bitsize))
            raise ValueError('not enough bits to generate voucher amount')
        if bitsize - amount_bitsize < 10:
            logg.warning('difference between amount and bitsize of domain is {}, higher difference advisable to reduce collision risk!'.format(bitsize - amount_bitsize))

        self.bitsize = bitsize
        self.bytesize = int(((self.bitsize - 1) / 8) + 1)
        self.mask = bytearray(self.bytesize)
        for i in range(self.bitsize):
            j = self.bytesize - int(i / 8) 
            self.mask[j-1] |= (1 << (i % 8))


    def generate(self, callback, spender_address=None):
        strikes = 0
        target = self.amount - self.number_generated
        i = 0
        while True:
            logg.debug('i {}'.format(i))
            if i == target:
                break
            v = os.urandom(self.bytesize)
            b = bytearray(self.bytesize)
            for j in range(self.bytesize):
                b[j] = v[j] & self.mask[j]
            b = self.fltr(bytes(b))
            value = self.value_generator.next()
            voucher = Voucher(b, value, self.spec, spender_address=spender_address)
            try:
                callback(i, voucher)
            except InvalidError as e:
                strikes += 1
                if strikes == self.strikes_limit:
                    logg.error('bailing on {} rejected values in a row'.format(strikes))
                    raise RuntimeError('bailing on {} rejected values in a row'.format(strikes))
                logg.warning('callback rejected our value "{}" with error: {}'.format(v, e))
                logg.debug('i {}'.format(i))
                continue
            i += 1
            strikes = 0
