# standard imports
import logging

# external imports
import celery
from chainlib.eth.pytest.fixtures_signer import agent_roles
from cic_eth.task import BaseTask
from chainlib.eth.nonce import RPCNonceOracle
from chainlib.eth.tx import receipt
from eth_erc20 import ERC20

# local imports
from cic_voucher.custodial import (
        VoucherBatch,
        VoucherStaticValueGenerator,
        )

logg = logging.getLogger()


def test_redeem_task_novoucher(
        default_chain_spec,
        init_database,
        init_celery_tasks,
        celery_session_worker,
        celery_voucher_store,
        default_voucher_spec,
        agent_roles,
        foo_token,
        custodial_roles,
        eth_rpc,
        eth_signer,
        ):


    value = 1024
    generator = VoucherStaticValueGenerator(value, 10)
    voucher_batch = VoucherBatch(default_voucher_spec, generator, bitsize=8)
    voucher_batch.generate(celery_voucher_store.callback, spender_address=agent_roles['ALICE'])

    holder = default_voucher_spec.holder_address
    nonce_oracle = RPCNonceOracle(holder, conn=eth_rpc)
    c = ERC20(default_chain_spec, signer=eth_signer, nonce_oracle=nonce_oracle) 
    (tx_hash, o) = c.approve(foo_token, holder, agent_roles['ALICE'], value)
    r = eth_rpc.do(o)
    o = receipt(tx_hash)
    r = eth_rpc.do(o)

    assert r['status'] == 1

    ks = celery_voucher_store.keys()

    logg.debug('basetask {} {}'.format(BaseTask.call_address, BaseTask))
    s = celery.signature(
            'cic_voucher.custodial.task.redeem.redeem_voucher',
            [
                ks[0],
                agent_roles['BOB'],
                ]
            )
    t = s.apply_async()
    t.get_leaf()
    assert t.successful()
