class VoucherPresetGenerator:

    def __init__(self, amounts):
        self.amounts = amounts


    def count(self):
        return len(self.amounts)


    def next(self):
        return self.amounts.pop(0)
