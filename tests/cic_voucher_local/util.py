# standard imports
import logging

# local imports
from cic_voucher.error import (
        InvalidError,
        DoubleSpendError,
        )
from cic_voucher.store import VoucherStoreIterator
from cic_voucher.generate import VoucherPresetGenerator
from cic_voucher.filter import (
        number_filter,
        )
from cic_voucher.custodial.voucher import Voucher

logg = logging.getLogger(__name__)


def limit_number_filter(v):
    s = number_filter(v)
    assert int(s) < 256
    return s


class VoucherLimitStore:
    
    def __init__(self, limit=0):
        self.store = {}
        self.limit = limit - 1


    def peek(self, k, session=None):
        return self.store[k]


    def get(self, k, session=None):
        v = self.peek(k)
        if v.check_expired():
            raise ExpiredError(k)
        if v.check_used():
            raise DoubleSpendError(k)
        v.set_used()
        return v


    def peek(self, k, session=None):
        return self.store[k]


    def __len__(self):
        return len(self.store.keys())
  

    def keys(self):
        return list(self.store.keys())


    def callback(self, idx, voucher):
        if self.limit > 0 and idx == self.limit:
            raise InvalidError('limit exceeded')
        logg.debug('store received voucher number {}: {}'.format(idx, repr(voucher)))
        self.store[str(voucher)] = voucher


    def __iter__(self):
        return VoucherStoreIterator(self.store)
