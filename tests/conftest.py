# external imports
import pytest
from cic_eth_registry.pytest.fixtures_tokens import *
from cic_eth_registry.pytest.fixtures_contracts import *
from chainlib.eth.pytest.fixtures_chain import *
from chainlib.eth.pytest.fixtures_ethtester import eth_rpc
from cic_eth_registry.erc20 import ERC20Token
from eth_erc20 import ERC20
from cic_eth.pytest.fixtures_database import *
from cic_eth.pytest.fixtures_config import *
from cic_eth.pytest.fixtures_role import *

# local imports
from cic_voucher.voucher import VoucherSpec

# tests_imports
from tests.fixtures_celery import *


@pytest.fixture(scope='function')
def default_voucher_spec(
        token_roles,
        eth_rpc,
        foo_token,
        default_chain_spec,
        register_tokens,
        register_lookups,
        ):

    token = ERC20Token(default_chain_spec, eth_rpc, foo_token)
    token.load(eth_rpc)
    voucher_spec = VoucherSpec(default_chain_spec, token.symbol, token_roles['FOO_TOKEN_OWNER'])
    return voucher_spec


@pytest.fixture(scope='function')
def foo_token_symbol(
    default_chain_spec,
    foo_token,
    eth_rpc,
    contract_roles,
    ):
    
    c = ERC20(default_chain_spec)
    o = c.symbol(foo_token, sender_address=contract_roles['CONTRACT_DEPLOYER'])
    r = eth_rpc.do(o)
    return c.parse_symbol(r)
