# standard imports
import logging
import copy

# external imports
import pytest

# local imports
from cic_voucher import VoucherSpec
from cic_voucher.custodial import (
        VoucherBatch,
        VoucherStaticValueGenerator,
        )
from cic_voucher.filter import (
        number_filter,
        PadFilter,
        )

# test imports
from tests.cic_voucher_local.util import (
        limit_number_filter,
        VoucherLimitStore,
        VoucherPresetGenerator,
        )

logg = logging.getLogger()


def test_offline_voucher(
        default_chain_spec,
        contract_roles,
        default_voucher_spec,
        ):

    generator = VoucherStaticValueGenerator(1024, 10)
    voucher_batch = VoucherBatch(default_voucher_spec, generator, bitsize=8)
    voucher_store = VoucherLimitStore()
    voucher_batch.generate(voucher_store.callback)


def test_offline_voucher_limit(
        default_chain_spec,
        contract_roles,
        default_voucher_spec,
        ):

    generator = VoucherStaticValueGenerator(1024, 10)
    voucher_batch = VoucherBatch(default_voucher_spec, generator, bitsize=8, identifier_filter=limit_number_filter)
    voucher_store = VoucherLimitStore()
    voucher_store = VoucherLimitStore(limit=9)
    with pytest.raises(RuntimeError):
        voucher_batch.generate(voucher_store.callback)


def test_static_amounts_voucher(
        default_chain_spec,
        contract_roles,
        default_voucher_spec,
        ):

    amounts = [1024, 512, 13]
    generator = VoucherPresetGenerator(copy.copy(amounts))
    fltr = PadFilter(100, number_filter)
    voucher_batch = VoucherBatch(default_voucher_spec, generator, bitsize=160, identifier_filter=fltr.filter)
    voucher_store = VoucherLimitStore()
    voucher_batch.generate(voucher_store.callback)

    for i, k in enumerate(voucher_store.store.keys()):
        assert len(k) == 100
        assert voucher_store.store[k].value == amounts[i]


def test_prefix_label_voucher(
        default_chain_spec,
        contract_roles,
        ):

    spec = VoucherSpec(default_chain_spec, 'XXX', contract_roles['CONTRACT_DEPLOYER'], label='foo', prefix='bar')

    fltr = PadFilter(7, number_filter)
    generator = VoucherStaticValueGenerator(1024, 10)
    voucher_batch = VoucherBatch(spec, generator, bitsize=8, identifier_filter=fltr.filter)
    voucher_store = VoucherLimitStore()
    voucher_batch.generate(voucher_store.callback)

    for v in voucher_store.store.values():
        assert len(str(v)) == 10


def test_store_iterator(
        default_chain_spec,
        contract_roles,
        ):

    spec = VoucherSpec(default_chain_spec, 'XXX', contract_roles['CONTRACT_DEPLOYER'], label='foo', prefix='bar')

    generator = VoucherStaticValueGenerator(1024, 10)
    voucher_batch = VoucherBatch(spec, generator, bitsize=8)
    voucher_store = VoucherLimitStore()
    voucher_batch.generate(voucher_store.callback)

    i = 0
    for v in voucher_store:
        i += 1
    assert i == 10
