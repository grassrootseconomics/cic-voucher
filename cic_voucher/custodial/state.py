# standard imports
import enum

class VoucherState(enum.IntEnum):
    REDEEMABLE = 1
    USED = 2
    EXPIRED = 4
    REVOKED = 8
