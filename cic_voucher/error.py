class InvalidError(Exception):
    """Raised when callback rejects the randomly generated value from the component
    """
    pass


class DoubleSpendError(Exception):
    """Raised when an attempt is made to redeem a voucher that has already been used
    """
    pass


class ExpiredError(Exception):
    """Raised when an attempt is made to redeem a voucher that has expired
    """
    pass


class StateChangeError(Exception):
    """Raised when an invalid voucher state change is attempted
    """
    pass


