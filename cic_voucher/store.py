class VoucherStoreIterator:

    def __init__(self, store):
        self.store_keys = list(store.keys())
        self.count = len(store)
        self.store = store
        self.cursor = 0


    def __iter__(self):
        return self


    def __next__(self):
        if (self.cursor == self.count):
            raise StopIteration(self.count)
        c = self.cursor 
        self.cursor += 1
        return self.store.get(self.store_keys[c])
