# external imports
import pytest
from cic_eth.db.models.base import SessionBase
from cic_voucher.custodial.redeem import CustodialRedeemer

# local imports
from cic_voucher.custodial.task.redeem import VoucherTask

# test imports
from tests.cic_voucher_local.util import (
        VoucherLimitStore,
        )

VoucherTask.session_func = SessionBase.create_session


@pytest.fixture(scope='function')
def celery_voucher_store(
        default_chain_spec,
        default_voucher_spec,
        ):

    store = VoucherLimitStore()
    VoucherTask.redeemer = CustodialRedeemer(default_chain_spec, default_voucher_spec, store)
    return store
